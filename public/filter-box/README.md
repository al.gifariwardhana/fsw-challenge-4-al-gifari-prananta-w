# Latihan DOM (Document Object Model)
Latihan kali ini akan mengajak kamu memahami DOM (Document Object Model), apa saja isi latihan kali ini?
1. Pemahaman tentang DOM
2. Menggunakan DOM
3. Memanipulasi DOM

> oya, latihan kali ini bukan soal loh.  
> melainkan panduan untuk kamu ikuti dan praktek langsung dikomputer kamu.

Pertama-tama kamu perlu siapkan dulu bahannya ya:
1. vscode
2. browser
3. file zip yang sudah di share di group (extract kemudian buka di vscode)

yang akan kita buat kali ini adalah membuat susunan html 100% menggunakan pure DOM (Document Object Model). Yuk kita mulai langkah-langkahnya.

<div style="page-break-after:always"></div>

### 1. terdapat beberapa file yang ada di dalam nya, kamu cukup fokus ke file `filter-box.js` saja.

```bash
├── assets
│   ├── css
│   │   └── filter-box.css
│   ├── img
│   │   └── user-icon.svg
│   └── js
│       ├── filter-box-container.js
│       └── filter-box.js
└── index.html
```

### 2. buka file `filter-box.js` dengan vscode, kurang lebih isinya seperti berikut

```javascript
function createFilterBox() {
    // ... kode bawaan

    // definisikan isi form-group 1 disini ...

    // definisikan isi form-group 2 disini ...

    // definisikan isi form-group 3 disini ...

    // definisikan isi form-group 4 disini ...

    // definisikan isi form-group 5 disini ...
}

function onSearch(props) {
    console.log(props)
}

createFilterBox()
```

<div style="page-break-after:always"></div>

### 3. selanjutnya kita akan mulai definisikan isi dari form-group 1

> click gambar untuk melihat kode selengkapnya  
> atau click url ini [DOM Step 1](https://asciinema.org/a/34EOmd0EO4KP0dCp0xP6uy3Sx)

[![DOM Step 1](https://asciinema.org/a/34EOmd0EO4KP0dCp0xP6uy3Sx.png)](https://asciinema.org/a/34EOmd0EO4KP0dCp0xP6uy3Sx)

kalau sudah kamu isi dengan kode diatas, coba lihat tampilan index.html di browser.

<div style="page-break-after:always"></div>

### 4. setelah form-group 1 berhasil dibuat, langkah selanjutnya mendefinisikan isi form-group ke 2

> click gambar untuk melihat kode selengkapnya  
> atau click url ini [DOM Step 2](https://asciinema.org/a/M5M1NZmL5pZAKJkTtUqmxOs9y)

[![DOM Step 2](https://asciinema.org/a/M5M1NZmL5pZAKJkTtUqmxOs9y.png)](https://asciinema.org/a/M5M1NZmL5pZAKJkTtUqmxOs9y)

kalau sudah kamu isi dengan kode diatas, coba lihat tampilan index.html di browser.

<div style="page-break-after:always"></div>

### 5. sekarang lanjut definisikan form-group ke 3

> click gambar untuk melihat kode selengkapnya  
> atau click url ini [DOM Step 3](https://asciinema.org/a/yrN21o6xINaaFuyufsByp4w66)

[![DOM Step 3](https://asciinema.org/a/yrN21o6xINaaFuyufsByp4w66.png)](https://asciinema.org/a/yrN21o6xINaaFuyufsByp4w66)

kalau sudah kamu isi dengan kode diatas, coba lihat tampilan index.html di browser.

<div style="page-break-after:always"></div>

### 6. lannjutkan kembali definisikan form-group ke 4

> click gambar untuk melihat kode selengkapnya  
> atau click url ini [DOM Step 4](https://asciinema.org/a/n3qRKP4ijPZjI98YYUjhMwMeU)

[![DOM Step 4](https://asciinema.org/a/n3qRKP4ijPZjI98YYUjhMwMeU.png)](https://asciinema.org/a/n3qRKP4ijPZjI98YYUjhMwMeU)

kalau sudah kamu isi dengan kode diatas, coba lihat tampilan index.html di browser.

<div style="page-break-after:always"></div>

### 7. langkah terakhir membuat definisi dari form-group ke 5

> click gambar untuk melihat kode selengkapnya  
> atau click url ini [DOM Step 5](https://asciinema.org/a/3RGgGzSSFpjvkVPx0zA220Cu6)

[![DOM Step 5](https://asciinema.org/a/3RGgGzSSFpjvkVPx0zA220Cu6.png)](https://asciinema.org/a/3RGgGzSSFpjvkVPx0zA220Cu6)

kalau sudah kamu isi dengan kode diatas, coba lihat tampilan index.html di browser. 

## Penutup
Kamu sudah latihan membuat dan memanipulasi dom. Sekarang tentu kamu sudah paham kan konsep DOM itu bagaimana.
**Happy Hacking!!**

